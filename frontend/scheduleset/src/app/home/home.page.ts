import { FeedService } from './../services/feed.service';
import { Component ,OnInit} from '@angular/core';
import { ModalController ,LoadingController, AlertController} from '@ionic/angular';  
import * as moment from 'moment';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  schedulesets : any;
  days : any; 
  preDelay : any;
  rushDelay : any;
  nextOrderDate : any;
  refreshTimer : any;
  constructor(public modalCtrl : ModalController,
    public loadingController : LoadingController,
    public alertController: AlertController, 
    public feedService : FeedService) {
      this.days = [  "MON","TUE","WED","THU","FRI","SAT","SUN" ];
      this.preDelay = 20;
      this.rushDelay = 10; 
      this.refreshTimer = setInterval(this.calcuNextOrderDate.bind(this), 1500);

     }
  ngOnInit() {
    this.fetchAll();
    this.calcuNextOrderDate();
  }
  async fetchAll(){
    const loading = await this.loadingController.create({
      message: 'Loading...'
    });
    await loading.present();
    await this.feedService.fetchAll() 
      .subscribe(res => {
        this.schedulesets = res.data;
        console.log(this.schedulesets);
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      }); 
  } 
  toggleSection(i) {
    this.schedulesets[i].open = !this.schedulesets[i].open;
  }
 
  toggleItem(i, j) {
    this.schedulesets[i].schedules[j].open = !this.schedulesets[i].schedules[j].open;
  }

  async updateRemote(scheduleset){
    const loading = await this.loadingController.create({
      message: 'Loading...'
    });
    await loading.present();
    await this.feedService.update(scheduleset) 
      .subscribe(res => { 
        loading.dismiss();
        this.presentAlert("Info",'Scheduleset updated.');
      }, err => {
        console.log(err);
        this.presentAlert("Error",'Scheduleset update failed.');
        loading.dismiss();
      });   
  }
   
  deleteScheduleRemote(scheduleset,scheduleId){
    const index = scheduleset.schedules.findIndex(s => s._id === scheduleId); 
    scheduleset.schedules.splice(index, 1);
    this.updateRemote(scheduleset);  
  }
  async presentAlert(header,message) {
    const alert = await this.alertController.create({
      header: header, 
      message: message,
      buttons: ['OK']
    }); 
    await alert.present();
  }
  calcuStart(schedule){
    let start = schedule.start;
    return start/15;
  }
  calcuEnd(schedule){
    let end = schedule.end;
    return end/15
  }
  
  onStartChange($event,schedule){
    let s = Number($event.detail.value);
    if(s<1)
     return; 
    schedule.start = s*15;
  }
  onEndChange($event,schedule){
    let s = Number($event.detail.value);
    if(s<1)
     return; 
    schedule.end = s*15;
  }
  async deleteScheduleSetRemote(scheduleset){
    const loading = await this.loadingController.create({
      message: 'Loading...'
    });
    await loading.present();
    await this.feedService.delete(scheduleset) 
      .subscribe(res => { 
        loading.dismiss();
        this.deleteScheduleSet(this.schedulesets,scheduleset);
        this.presentAlert("Info",'Scheduleset deleted.');
      }, err => {
        console.log(err);
        this.presentAlert("Error",'Scheduleset delete failed.');
        loading.dismiss();
      }); 
  }
  deleteScheduleSet(schedulesets,scheduleset){
    const index = schedulesets.findIndex(s => s._id === scheduleset._id); 
    schedulesets.splice(index, 1); 
  }
  delayValidation(){
    return (this.preDelay + this.rushDelay ) % 15 == 0; 
  }
  calcuNextOrderDate(){
    let ctx = this;
    const  durationInMinutes = ctx.preDelay + ctx.rushDelay + 15;
    const now = moment();
    const nextOrderDate = moment(now, 'HH:mm:ss').add(durationInMinutes, 'minutes').format('HH:mm'); 
    this.nextOrderDate = nextOrderDate; 
    this.delayValidation();
  }
  onDelayChange(){
    this.delayValidation();
    this.calcuNextOrderDate();
  }
  
}
