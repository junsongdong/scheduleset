import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ResponseService } from './response.service';
import { catchError, tap, map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
 
@Injectable({
  providedIn: 'root'
})
export class FeedService {

  constructor(private http : HttpClient,public responseService : ResponseService) { }

  fetchAll() : Observable <any> {
    return this.http.get<any[]>(environment.SERVER_URL + "/api/feed/schedulesets")
    .pipe(
      tap(users => console.log('fetched schedulesets')),
      catchError(this.responseService.handleError('fetchAll', []))
    );
  }
  fetchOne(){

  }
  update(scheduleset) : Observable <any>{
    return this.http.put<any>(environment.SERVER_URL +  "/api/feed/scheduleset/" + scheduleset._id, scheduleset, httpOptions).pipe(
      tap( category => console.log('updated scheduleset')),
      catchError(this.responseService.handleError<any>('update'))
    );
  }
  delete(scheduleset) : Observable <any>{
    return this.http.delete<any>(environment.SERVER_URL + "/api/feed/scheduleset/"+ scheduleset._id, httpOptions).pipe(
      tap( user => console.log('deleted scheduleset')),
      catchError(this.responseService.handleError<any>('delete'))
    );
  }
}
