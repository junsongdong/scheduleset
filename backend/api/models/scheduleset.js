const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const SchedulesetSchema = new Schema(
  { 
    schedules: [
        {
            scheduleId: {
                type: String,
                required: true
              },
              day:{
                type: String,
                required: true
              },
              start: {
                type: Number,
                required: true
              },
              end: {
                type: Number,
                required: true
              } 
        }
      ]
  }, 
  { timestamps: true }
);
module.exports = mongoose.model("Scheduleset", SchedulesetSchema);
 