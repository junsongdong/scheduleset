const Scheduleset = require("../models/scheduleset");
exports.create = async (req, res, next) => {
  try {
    const schedules = req.body.schedules; 
    const scheduleset = new Scheduleset({
      schedules: schedules 
    });
    let newScheduleset = await scheduleset.save(); 
    res.status(201).json({
      message: "scheduleset created successfully",
      data: newScheduleset
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.fetchAll = async (req, res, next) => {
  try {
    let schedulesets = await Scheduleset.find();
    if (schedulesets.length == 0) {
      const error = new Error("schedulesets not found ");
      error.statusCode = 404; 
      throw error;
    }
    res.status(200).json({
      message: "schedulesets found successfully", 
      data: schedulesets 
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};
exports.fetchOne = async (req, res, next) => {
  try {
    const id = req.params.id;
    let scheduleset = await Scheduleset.findById(id);
    if (!scheduleset) {
      const error = new Error("Scheduleset not found");
      error.statusCode = 404;
      throw error;
    }
    res.status(200).json({
      message: "scheduleset found",
      data: scheduleset 
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};
exports.update = async (req, res, next) => {
  try {
    const id = req.params.id; 
    const schedules = req.body.schedules;  
    let scheduleset = await Scheduleset.findById(id);
    if (!scheduleset) {
      const error = new Error("scheduleset not found");
      error.statusCode = 404; 
      throw error;
    }
    scheduleset.schedules = schedules; 
    let newScheduleset = await scheduleset.save();
    res.status(200).json({
      message: "scheduleset updated", 
      data: newScheduleset  
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};
exports.delete = async (req, res, next) => {
  try {
    const id = req.params.id;
    let scheduleset = await Scheduleset.findById(id);
    if (!scheduleset) {
      const error = new Error("scheduleset not found");
      error.statusCode = 404; 
      throw error;
    }
    scheduleset = await Scheduleset.findByIdAndRemove(id);
    res.status(200).json({
      message: "scheduleset deleted", 
      data: scheduleset 
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};
