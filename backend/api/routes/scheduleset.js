const express = require("express");
const router = express.Router();
const schedulesetController = require("../controllers/schedulesetController");

router.post("/scheduleset", schedulesetController.create);
router.get("/schedulesets", schedulesetController.fetchAll); 
router.get("/scheduleset/:id", schedulesetController.fetchOne); 
router.put("/scheduleset/:id", schedulesetController.update);
router.delete("/scheduleset/:id", schedulesetController.delete); 
module.exports = router;