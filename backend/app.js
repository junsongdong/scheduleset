const express = require('express');
const bodyParser = require('body-parser'); 
const mongoose = require('mongoose');  
const app = express();  
const schedulesetRouter = require('./api/routes/scheduleset');

app.use(bodyParser.json({ extended: true }));   
app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    next();
});
 app.use('/api/feed' , schedulesetRouter);  

app.use((error, req, res, next) => {
    console.error(error);
    const status = error.statusCode || 500;
    const message = error.message;
    const data = error.data
    res.status(status).json({message : message,data : data}); 
})
//database
mongoose.connect('mongodb://scheduleset:scheduleset@cluster0-shard-00-00-xvhtw.mongodb.net:27017,cluster0-shard-00-01-xvhtw.mongodb.net:27017,cluster0-shard-00-02-xvhtw.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true',{ useNewUrlParser: true }).then( result => {
    app.listen(3000);
    console.log("app listening....");
}).catch(error =>{
    console.log(error);
});

